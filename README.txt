Copy "steamapps\common\Half-Life 2 Update\hl2\" into "steamapps\sourcemods\" and rename the directory to "HL2Update".
Then copy "gameinfo.txt" and "resource" into "steamapps\sourcemods\HL2Update\" and overwrite existing files.

The idea is to copy the patched game contents from "Half-Life 2: Update" and run the game as a source mod using the
game dll from the episode titles (because they support HDR). If the episode titles are uncut, the new source mod should be too.
The game's title in Steam is "Half-Life 2: Update (uncut)".

Files:
resource\HL2Update_{english|german}.txt - added HL2Update_ prefixes so the chapter titles and similar captions are visible; only German and English (sorry)
resource\gamemenu.res - achievements menu entry removed since it wasn't working
resource\update.tga - game icon used in Steam
gameinfo.txt - modified to use the episode dlls

